use strict;
use MARC::Batch;
use MARC::Record;
use MARC::File::USMARC;
use utf8;
use Text::CSV;

my $pdf_dir = $ARGV[2];

open( OUTPUT_CSV_BIB, '> ' . $ARGV[1] . "bib.csv" ) or die $!;
open( OUTPUT_CSV_PDF, '> ' . $ARGV[1] . "pdf.csv") or die $!;
open( OUTPUT_LOG , '> log.txt' ) or die $!;

my $batch;

my $DEBUG = 0;
my $current_record = "";
binmode(OUTPUT_LOG, ':utf8');
binmode(STDOUT, ':utf8');

my @bibnos = ();
my @pdfs = ();
my @pdfs_match = ();

#----------------------------------------------------
opendir(DIR, $ARGV[0] ) or die $!;

while (my $file = readdir(DIR)) 
{
	if( index( lc $file,".mrc") >= 0 )
	{
		print $file . "\n";
		$batch = MARC::Batch->new('USMARC',$ARGV[0] . $file);
		$batch->strict_off();
		$batch->warnings_off();
		listMARCtoList();
	}
}
closedir(DIR);
#----------------------------------------------------
@bibnos = sort @bibnos;
listPDFtoList();
checkPDF_and_Bibno();

close( OUTPUT_CSV_BIB );
close( OUTPUT_CSV_PDF );
close( OUTPUT_LOG );

sub listMARCtoList
{
	my $record;
	eval { $record = $batch->next(); };
    while ( $record )
    {

    	my $bibno = $record->field("001")->as_string();
    	$bibno =~ s/000//g if( index($bibno,"000") == 0 );
    	$bibno =~ s/00//g if( index($bibno,"00") == 0 );
    	$bibno =~ s/-//g;

        push @bibnos , $bibno;
	    
	    READ_BATCH:
	    if ( my @warnings = $batch->warnings() ) 
	    {
           plog( "WARNINGS :" . @warnings . "\n" );
        }
	    do 
	    {
           eval { $record = $batch->next(); };
           if ($@){ plog( "ERROR next to bib#  (skipped):\n" . $@ . "\n"); }
        } 
        while $@;
    }     
}

sub listPDFtoList
{
	opendir(DIR, $pdf_dir ) or die $!;

	while (my $file = readdir(DIR)) 
	{
		if( index( lc $file,".pdf") >= 0 )
		{
			push @pdfs , $file;
		}
	}
	
	closedir(DIR);
}

sub checkPDF_and_Bibno
{
	print OUTPUT_CSV_BIB "\"bibno\",\"status\",\"pdf name\"\n";
	foreach my $bibno (@bibnos) 
	{
		my $match = 0;
		foreach my $pdf (@pdfs) 
		{
			my $pdf_for_check = lc $pdf;
		    $pdf_for_check =~ s/.pdf//g;
			if( $pdf_for_check eq $bibno )
			{
				print OUTPUT_CSV_BIB "\"$bibno\",\"match\",\"$pdf\"\n";
				push @pdfs_match , $pdf;
				$match = 1;
				next;
			}
		}
		print OUTPUT_CSV_BIB "\"$bibno\",\"not match\",\"\"\n" if ! $match;
	}
	print OUTPUT_CSV_PDF "\"pdf\",\"status\"\n";
	@pdfs = sort @pdfs;
	foreach my $pdf (@pdfs) 
	{
		my $match = 0;
		foreach my $pdf_match (@pdfs_match) 
		{
			if( $pdf eq $pdf_match )
			{
				$match = 1;
				next;
			}
		}
		print OUTPUT_CSV_PDF "\"$pdf\",\"not match bib\"\n" if ! $match;
	}
}

sub removeFromArr
{
	my ($element) = @_;
	@pdfs = grep $element, @pdfs;
}

sub plog 
{
	my ($log) = @_;
	foreach my $x (@ARGV) 
	{
	  	if ($x eq '-d' || $x eq '-D') 
	  	{
	  	 	$DEBUG = 1;
	  	}
	}
	print OUTPUT_LOG $log if $DEBUG; 	
}









































 
    