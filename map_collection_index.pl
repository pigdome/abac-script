use strict;
use MARC::Batch;
use MARC::Record;
use MARC::File::USMARC;
use utf8;
use Text::CSV;

my $index = "/srv/work/pdf_renamed/index/index-fin.csv";
my $map_collection = "/srv/work/pdf_renamed/index/collectionID-mapping-v-2.csv";

binmode(STDOUT, ':utf8');



my $csv = Text::CSV->new ( { binary => 0 } ) or die "Cannot use CSV: ".Text::CSV->error_diag ();
open(my $fh_index, '<:encoding(UTF-8)', $index) or die "Could not open file '$index' $!";
open(my $fh_map_collection, '<:encoding(UTF-8)', $map_collection) or die "Could not open file '$map_collection' $!";

my @rows_index = ();
my @rows_map_index = ();

#-----------------------------------------------------

while ( my $row = $csv->getline( $fh_map_collection ) )
{
	push @rows_map_index , $row;
}
close $fh_map_collection;

while ( my $row = $csv->getline( $fh_index ) )
{
	$row->[33] =~ s/^\s+|\s+$//g;
	my $citation = $row->[33];
	#print $citation . "\n";

	foreach my $row_map_index (@rows_map_index) 
	{
		my $map_citation = $row_map_index->[0];
		$map_citation =~ s/^\s+|\s+$//g;#print $citation . ": " . $map_citation . "\n";
		#print $row_map_index->[0] . "\n";
		if( index($citation,$map_citation) >= 0 )
		{
			$row->[0] = $row_map_index->[1];#print $row_map_index->[1] . "\n";
			last;
		}
	}

	push @rows_index , $row;
}
close $fh_index;
#-----------------------------------------------------

open( my $OUTPUT_CSV, '> /srv/work/pdf_renamed/index/index-fin-map.csv' ) or die $!;
binmode($OUTPUT_CSV, ':utf8');

# foreach my $row_index (@rows_index) 
# {
# 	#print OUTPUT_CSV join(',', @$row_index) . "\n";
# }
$csv->eol ("\r\n");
$csv->print ($OUTPUT_CSV, $_) for @rows_index;
close( $OUTPUT_CSV );














































 
    