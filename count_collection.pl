#use File::Find::Rule;
use strict;
use warnings;

my $basedir = $ARGV[0];

opendir(HOMEDIR,$basedir) || die ("Unable to open directory");
open FILE, "> /srv/work/collection/count_c" or next;
while (my $filename = readdir(HOMEDIR)) 
{ 
    my $subdir = $basedir. "/" . $filename;
    
    if( -d $subdir )
    {
    	unless ($filename eq '.' or $filename eq '..') 
    	{
    		
    		my $count = 0;
    		opendir(SUBDIR,$subdir) || die ("Unable to open directory");

    		while (my $filename_sub = readdir(SUBDIR)) 
			{
				$count++ if $filename_sub =~ /[0-9]/;
			}
			#$count -= 3;
			print FILE "hash['$filename'] = $count;\n";
			
    		closedir(SUBDIR);
    	}
    }
    
}
close( FILE );
closedir(HOMEDIR);