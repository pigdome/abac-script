#use File::Find::Rule;
use strict;
use warnings;

my $basedir = $ARGV[0];
opendir(HOMEDIR,$basedir) || die ("Unable to open directory");
my $count = $ARGV[1];

while (my $filename = readdir(HOMEDIR)) 
{ 
    unless ($filename eq '.' or $filename eq '..') 
    {
        open(INPUT, '<', $basedir . "/". $filename) or next;
        my @rows = ();
        while (my $line = <INPUT>) 
        {
            $line =~ s/^\s+|\s+$//g;
            #print $line;
            push @rows , $line;
        }
        close INPUT;
        open( OUTPUT_LOG , "> $basedir/$filename" ) or die $!;
        foreach my $line (@rows) 
        {
            print OUTPUT_LOG $line . "\n";
        }
        close OUTPUT_LOG;
    }
}
closedir(HOMEDIR); 