use strict;
use MARC::Batch;
use MARC::Record;
use MARC::File::USMARC;
use utf8;
use Text::CSV;



my $base_bitstream_dir = $ARGV[2];


open( OUTPUT_CSV, '> ' . $ARGV[1] ) or die $!;
open( OUTPUT_LOG , '> log.txt' ) or die $!;

my $batch = MARC::Batch->new('USMARC',$ARGV[0]);
$batch->strict_off();
$batch->warnings_off();

my $DEBUG = 0;
my $current_record = "";
my $has_no_country = 0;
my %row = ();

binmode(OUTPUT_CSV, ':utf8');
binmode(OUTPUT_LOG, ':utf8');
binmode(STDOUT, ':utf8');

my $output_header = "\"filename\",\"dc.title\",\"au.identifier.bibno\",\"au.identifier.other\",\"dc.title.alternative\",\"dc.date.created\",\"dc.date.issued\",\"dc.language.iso\",\"dc.identifier.isbn\",\"dc.identifier.nlm\",\"dc.contributor.author\",\"dc.contributor.other\",\"dc.subject\",\"dc.subject.ddc\",\"dc.subject.other\",\"dc.publisher\",\"dc.coverage.spatial\",\"dc.description\",\"dc.format.extent\",\"mods.edition\",\"mods.genre\",\"dc.relation.ispartofseries\",\"dc.relation.ispartof\",\"dc.relation.haspart\",\"dc.identifier.uri\",\"dc.type\",\"dc.format.mimetype\",\"mods.location.physicalLocation\",\"thesis.degree.name\",\"au.link.externalLink\",\"au.journal.department\",\"dc.rights\",\"dc.identifier.citation\",\"au.identifier.collection\"\n";
print OUTPUT_CSV $output_header;	  

my $section_csv = "/srv/work/convert/TMP_thesis/index/section.csv";
my $list_thesis_csv = "/srv/work/convert/TMP_thesis/index/list_index.csv";
my $name_map_csv = "/srv/work/convert/TMP_thesis/name_map.csv";

my $csv = Text::CSV->new ( { binary => 0 } ) or die "Cannot use CSV: ".Text::CSV->error_diag ();
open(my $fh_section, '<:encoding(UTF-8)', $section_csv) or die "Could not open file '$section_csv' $!";
open(my $fh_list, '<:encoding(UTF-8)', $list_thesis_csv) or die "Could not open file '$list_thesis_csv' $!";
open(my $fh_name_map, '<:encoding(UTF-8)', $name_map_csv) or die "Could not open file '$list_thesis_csv' $!";

#-----------------------------------------------------
my %hash_list = ();
while ( my $row = $csv->getline( $fh_list ) )
{
	$row->[1] =~ s/^\s+|\s+$//g;
	#$row->[2] =~ s/^\s+|\s+$//g;
	$row->[4] =~ s/^\s+|\s+$//g;

    $hash_list{$row->[1]}{'section'} = $row->[4];
}
close $fh_list;

my %hash_section = ();
while ( my $row = $csv->getline( $fh_section ) )
{
	$row->[1] =~ s/^\s+|\s+$//g;
	$row->[2] =~ s/^\s+|\s+$//g;

    $hash_section{$row->[1]} = $row->[2];
}
close $fh_section;

my %hash_name_map = ();
while ( my $row = $csv->getline( $fh_name_map ) )
{
	$row->[0] =~ s/^\s+|\s+$//g;
	$row->[1] =~ s/^\s+|\s+$//g;
	$row->[2] =~ s/^\s+|\s+$//g;

    $hash_name_map{$row->[0]}{'name'} = $row->[1];
    $hash_name_map{$row->[0]}{'discipline'} = $row->[2];
}
close $fh_name_map;
#-----------------------------------------------------


convertMARCToCSV();

close( OUTPUT_CSV );
close( OUTPUT_LOG );

sub convertMARCToCSV
{
	my $record;
	eval { $record = $batch->next(); };
    while ( $record )
    {
    	$current_record = $record->field("001")->as_string();
    	foreach my $field ($record->field("...")) 
	    {
	       my $log;
	       my @Field = ();
	       if( $field->tag() < 10 )
	       {
	       	   getFieldLower10( $field , $record->field( $field->tag() ) ) ;
	       }
	       else
	       {
    		   getFieldUpper10( $field );
	       }
	    }

	    $row{'filename'} = getFilename( $record->field("856") );
        autofill();
	    my %hash_temp = ();
        
        # group by tag
	    foreach my $key ( keys %row )
	    {
	    	my $data = $row{$key};
	    	if( $hash_temp{$key} )
	    	{ 
	    		$hash_temp{$key} = $hash_temp{$key} . "||" . $data;
	    	}
	    	else
	    	{
	    		$hash_temp{$key} = $data;
	    	}
	    }
        %row = %hash_temp;
	    # print 
	    my $output_data = "\"$row{'filename'}\",\"$row{'dc.title'}\",\"$row{'au.identifier.bibno'}\",\"$row{'au.identifier.other'}\",\"$row{'dc.title.alternative'}\",\"$row{'dc.date.created'}\",\"$row{'dc.date.issued'}\",\"$row{'dc.language.iso'}\",\"$row{'dc.identifier.isbn'}\",\"$row{'dc.identifier.nlm'}\",\"$row{'dc.contributor.author'}\",\"$row{'dc.contributor.other'}\",\"$row{'dc.subject'}\",\"$row{'dc.subject.ddc'}\",\"$row{'dc.subject.other'}\",\"$row{'dc.publisher'}\",\"$row{'dc.coverage.spatial'}\",\"$row{'dc.description'}\",\"$row{'dc.format.extent'}\",\"$row{'mods.edition'}\",\"$row{'mods.genre'}\",\"$row{'dc.relation.ispartofseries'}\",\"$row{'dc.relation.ispartof'}\",\"$row{'dc.relation.haspart'}\",\"$row{'dc.identifier.uri'}\",\"$row{'dc.type'}\",\"$row{'dc.format.mimetype'}\",\"$row{'mods.location.physicalLocation'}\",\"$row{'thesis.degree.name'}\",\"$row{'au.identifier.uri'}\",\"$row{'au.journal.department'}\",\"$row{'dc.rights'}\",\"$row{'dc.identifier.citation'}\",\"$row{'au.identifier.collection'}\"\n";

        print OUTPUT_CSV $output_data;
	    # clear for next record
        %row = ();
        $has_no_country = 1;
	    
	    READ_BATCH:
	    if ( my @warnings = $batch->warnings() ) 
	    {
           plog( "WARNINGS :" . @warnings . "\n" );
        }
	    do 
	    {
           eval { $record = $batch->next(); };
           if ($@){ plog( "ERROR next to bib#  (skipped):\n" . $@ . "\n"); }
        } 
        while $@;

    }
        
}


sub getFieldUpper10
{
	my ( $field ) = @_;
	my $tag = $field->tag();
	my @subfields = $field->subfields();
    my %hash = ();
    my $count = 0;
	while ( my $subfield = shift( @subfields ) ) 
	{

		my ( $code , $data ) = @$subfield;

		if( ( $tag >= 600 and $tag < 700 ) and $code eq 'z' )
		{
			#print "650 $data \n";
			$row{'dc.coverage.spatial'} = $data if $has_no_country;
			$has_no_country = 0;
		}

		$code = $count++ if( $tag >= 600 and $tag < 653 );
		print OUTPUT_LOG "$current_record key $code data $data \n";
		if( $hash{$code} )
		{
			$hash{$code} = $hash{$code} . " || " . $data;
		}
		elsif( $code eq 'x' and $hash{$code} )
		{
			$hash{$code} = $hash{$code} . " -- " . $data;
		}
		else
		{
			$hash{$code} = $data;
		}
	}
	if( $tag eq "020" ) 		{ getIdentifier_isbn(%hash); }
	#elsif( $tag eq "060"	{ getIdentifier_nlm(%hash); }
	elsif( $tag eq "082")		{ getSubject_ddc(%hash); }
	elsif( $tag eq "090")		{ getIdentifier_other(%hash); }
	elsif( $tag eq "100")		{ getContributor_author(%hash); }
	elsif( $tag eq "110")		{ getContributor_author(%hash); }
	elsif( $tag eq "245")		{ getTitle(%hash); }
	elsif( $tag eq "246")		{ getTitle_alternative(%hash); }
	elsif( $tag eq "250")		{ getMod_edition(%hash); }
	elsif( $tag eq "260")		{ getPublisherAndDate_issued(%hash); }
	#elsif( $tag eq "264")	{ getPublisherAndDate_issued(%hash); }
	elsif( $tag eq "300")		{ getFormat_extent(%hash); }
	elsif( $tag eq "330")		{ getFormat_extent_330(%hash); }
	elsif( $tag eq "440")		{ getRelation_ispartofseries(%hash); }
	elsif( $tag eq "500")		{ getDescription(%hash); }
	elsif( $tag eq "501")		{ getDescription_and_discipline(%hash); }
	elsif( $tag eq "502")		{ getDescription_and_discipline(%hash); }
	elsif( $tag eq "504")		{ getDescription(%hash); }
	elsif( $tag eq "520")		{ getDescription(%hash); }
	elsif( $tag eq "546")		{ getDescription(%hash); }
	elsif( $tag eq "563")		{ getDescription(%hash); }
	elsif( $tag eq "600")		{ getSubject_other(%hash); }
	elsif( $tag eq "610")		{ getSubject_other_610(%hash); }
	elsif( $tag eq "630")		{ getSubject_other(%hash); }
	elsif( $tag eq "649")		{}
	elsif( $tag eq "650")		{ getSubject_other_650(%hash); }
	elsif( $tag eq "651")		{ getSubject_other_650(%hash); }
	elsif( $tag eq "653")		{ getSubject(%hash); }
	elsif( $tag eq "700")		{ getContributor_author(%hash); }
	elsif( $tag eq "701")		{ getContributor_author(%hash); }
	elsif( $tag eq "710")		{ getContributor_other(%hash); }
	elsif( $tag eq "773")		{ getIdentifier_citation(%hash); }
	elsif( $tag eq "856")		{ getIdentifier_uri(%hash); }
	elsif( $tag eq "930")		{ getMod_genre(); }
	elsif( $tag eq "949")		{}
	elsif( $tag eq "998")		{}
	#elsif( $tag eq "999")		{ getThesis_degree_apartment(%hash); }
	else			{print OUTPUT_LOG "TAG $tag cant map";}
}




sub getFieldLower10
{
	my ( $field,$field_u10 ) = @_;
    my @Field = ();
    my $tag = $field->tag();
	my $data = $field_u10->as_string();
	#print OUTPUT_CSV "\"$tag\";;\"$data\"\n";
	if( $tag eq '008' )
	{
		get008( $data );
	}
	if( $tag eq '001' )
	{
		get001( $data );
	}
	if( $tag eq '003' )
	{
		get003( $data );
	}
}

sub get001
{
	my ($data) = @_;
	
	addToHash( 'au.identifier.bibno', $data );

    $data =~ s/000//g if( index($data,"000") == 0 );
    $data =~ s/00//g if( index($data,"00") == 0 );
    $data =~ s/-//g;
    
    my $section = $hash_list{$data}{'section'} if $hash_list{$data}{'section'};
    my $department = $hash_section{$section} if $section;	
    addToHash( 'au.journal.department', $department );
}

sub get003
{
	my ($data) = @_;
	addToHash( 'mods.location.physicalLocation' , $data );
}

sub getFilename
{
	my ($field) = @_;
	return "" if ! $field;
	my @subfields = $field->subfields();
	my $pdf_name;

	
	opendir(DIR, $base_bitstream_dir ) or die $!;

	while ( my $subfield = pop( @subfields ) ) 
	{
		
		my ( $code , $data ) = @$subfield;
		if( $code eq 'u' )
		{
			my $index_cut = rindex($data,'/');
			my $length = length($data)-$index_cut;
			$pdf_name = substr($data,$index_cut+1,$length);
			last;
		}
	}

	my $file;
	if( $pdf_name )
	{
		while ($file = readdir(DIR)) 
		{
			if( index($file,$pdf_name) >= 0 )
			{
				return $file; 
			}
		}
		print OUTPUT_LOG "$current_record cant map filename\n";
	}
	
	closedir(DIR);
	return $file;
}

sub get008
{
	my ($data) = @_;
	my $year = substr($data,0,2);
	my $month = substr($data,2,2);
	my $day = substr($data,4,2);
	my $issued = substr($data,7,4);
	my $language = substr($data,35,3);
	
	if( $year < 20 )
	{
		$year = "20" . $year;
	}
	else
	{
		$year = "19" . $year;
	}
    
    addToHash( 'dc.date.created' , $year . "-" . $month . "-" . $day );
    #addToHash( 'dc.date.issued' , $issued );
    addToHash( 'dc.language.iso' , $language );
}

sub getIdentifier_isbn
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	
	addToHash( 'dc.identifier.isbn' , $a );
}

sub getIdentifier_nlm
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	my $x = $hash{'x'};

	$a .= " -- " .$b if( $b ne '' );

	addToHash( 'dc.identifier.isbn' , $a );
}

sub getIdentifier_other
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	addToHash( 'au.identifier.other' , $a );
}

sub getIdentifier_citation
{
	my (%hash) = @_;
	my $t = $hash{'t'};
	my $b = $hash{'b'};
	my $g = $hash{'g'};
	$t .= " " .$b if( $b ne '' );
    $t .= " " .$g if( $g ne '' );
  
    addToHash( 'dc.identifier.citation' , $t );
    addToHash( 'au.identifier.collection' , $t );
}

sub getSubject_ddc
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	my $b = $hash{'b'};
	my $d = $hash{'d'};
    
    $a .= " " .$b if( $b ne '' );
    $a .= " " .$d if( $d ne '' );

	addToHash( 'dc.subject.ddc' , $a );
}

sub getSubject_other
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	addToHash( 'dc.subject.other' , $a );
}

sub getSubject_other_610
{
	my (%hash) = @_;
	my $output;
    
	foreach my $key ( sort keys %hash )
	{
		if( $output )
		{
			$output .= " -- " . $hash{$key};
		}
		else
		{
			$output = $hash{$key};
		}
		
	}

	addToHash( 'dc.subject.other' , $output );
}

sub getSubject_other_650
{
	my (%hash) = @_;
	my $output;
    
	foreach my $key ( sort keys %hash )
	{
		if( $output )
		{
			$output .= " -- " . $hash{$key};
		}
		else
		{
			$output = $hash{$key};
		}
		
	}

	addToHash( 'dc.subject.other' , $output );
}

sub getContributor_author
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	my $e = $hash{'e'};

	if( rindex($a,',') == (length($a)-1) )
	{
		$a = substr($a,0,(length($a)-2));
	}

	$a .= ", " . $e if $e;

	addToHash( 'dc.contributor.author' , $a );
}

sub getTitle
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	my $b = $hash{'b'};
	#my $c = $hash{'c'};
    
    if( rindex($a,"\/") == length($a)-1 )
    {
    	$a =~ s/\///g;
    }
    if( rindex($a,":") == length($a)-1 )
    {
    	$a =~ s/://g;
    }
    if( rindex($b,"\/") == length($b)-1 )
    {
    	$b =~ s/\///g;
    }
	$a .= " : " .$b if( $b ne '' );
    #$a .= " / " .$c if( $c ne '' );

	addToHash( 'dc.title' , $a );
}

sub getTitle_alternative
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	my $b = $hash{'b'};
	
	$a .= " : " .$b if( $b ne '' );

	addToHash( 'dc.title.alternative' , $a);
}

sub getCoverage
{
	my (%hash) = @_;
	my $output;
    
	foreach my $key ( sort keys %hash )
	{
		if( $output )
		{
			$output .= " -- " . $hash{$key};
		}
		else
		{
			$output = $hash{$key};
		}
	}
	
	addToHash( 'dc.coverage.spatial' , $output );
}

sub getMod_edition
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	
	addToHash( 'mods.edition' , $a );
}

sub getPublisherAndDate_issued
{
	my (%hash) = @_;
	#my $a = $hash{'a'};
	my $b = $hash{'b'};
	my $c = $hash{'c'};
	
	# if( rindex($a,":") == length($a)-1 )
 #    {
 #    	$a =~ s/://g;
 #    }
    if( rindex($b,",") == length($b)-1 )
    {
    	$b =~ s/,//g;
    }
	#$a .= " : " .$b if( $b ne '' );

	addToHash( 'dc.publisher' , $b );
	addToHash( 'dc.date.issued' , $c );
}

sub getFormat_extent
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	my $b = $hash{'b'};
	my $c = $hash{'c'};
	my $e = $hash{'e'};
    
    $a =~ s/^\s+|\s+$//g;
    #$a = substr($a,0,length($a)-1) if( rindex($a,'.') == length($a) );print rindex($a,'.') . ":" . length($a) . "\n";
    if( $b )
    {
    	#$b =~ s/.;/. ;/g;
    	#$b =~ s/.:/. :/g;
    	$b =~ s/^\s+|\s+$//g;
    	#$b = substr($b,0,length($b)-1) if( rindex($b,'.') == length($b) );print rindex($b,'.') . ":" . length($b) . "\n";
    	$a .= " " . $b;
    }
    if( $c )
    {
    	$c =~ s/^\s+|\s+$//g;
    	$a .= " " . $c;
    }
    if( $e )
    {
    	$e =~ s/^\s+|\s+$//g;
    	$a .= " " . $e;
    }
    $a =~ s/\.:/. :/g;
    $a =~ s/\.;/. ;/g;
	addToHash( 'dc.format.extent' , $a );
}

sub getFormat_extent_330
{
	my (%hash) = @_;
	my $a = $hash{'a'};	
	my $c = $hash{'c'};
    
    $a =~ s/^\s+|\s+$//g;
    
    if( $c )
    {
    	$c =~ s/^\s+|\s+$//g;
    	$a .= " " . $c;
    }
   
	addToHash( 'dc.format.extent' , $a );
}

sub getRelation_ispartofseries
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	
	addToHash( 'dc.relation.ispartofseries' , $a );	
}

sub getRelation_ispartof
{
	my (%hash) = @_;
  	my $t = $hash{'t'};
  	my $g = $hash{'g'};

  	$t .= " " .$g if( $g ne '' );

	addToHash( 'dc.relation.ispartof' , $t );	
}

sub getDescription
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	addToHash( 'dc.description' , $a );
}

sub getDescription_and_discipline
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	$a =~ s/--/ -- /g;
	addToHash( 'dc.description' , $a );
    
    my $parentheses_l = index($a,"(");
    my $parentheses_r;
    if( rindex($a,")") - index($a,")") > 10 )
    {
    	$parentheses_r = index($a,")");
    }
    else
    {
    	$parentheses_r = rindex($a,")");
    }
    


	if( $parentheses_l >= 0 and $parentheses_r >= 0 )
	{
		my $name = substr($a,$parentheses_l+1,($parentheses_r-$parentheses_l)-1);
		$name =~ s/^\s+|\s+$//g;
		
		if( $hash_name_map{$name}{'name'} )
		{
			addToHash( 'thesis.degree.name' , $hash_name_map{$name}{'name'} ) ;
			addToHash( 'thesis.degree.discipline' , $hash_name_map{$name}{'discipline'} );
		}
		else
		{
			addToHash( 'thesis.degree.name' , $name ) ;
			#addToHash( 'thesis.degree.discipline' ,  );
		}
	}
}

sub getContributor_other
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	my $b = $hash{'b'};

	$a .= " " .$b if( $b ne '' );

	addToHash( 'dc.contributor.other' , $a );	
}

sub getSubject
{
	my (%hash) = @_;
	my $output;
    
	foreach my $key ( sort keys %hash )
	{
		if( $output )
		{
			$output .= " -- " . $hash{$key};
		}
		else
		{
			$output = $hash{$key};
		}
		
	}

	addToHash( 'dc.subject' , $output );	
}

sub getContributor_auther
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	my $e = $hash{'e'};

	$a .= ", " . $e if( $e ne '' );

	addToHash( 'dc.contributoe_auther' , $a );	
}

sub getIdentifier_uri
{
	my (%hash) = @_;
	my $n = $hash{'n'};
	my $u = $hash{'u'};
	
	if( index($n,'Full') >= 0 )
	{
		addToHash( 'au.identifier.uri' , $n . " : " . $u );
	}
	else
	{
		addToHash( 'au.identifier.uri' , $n . " : " . $u );
	}
}

sub getMod_genre
{
	my (%hash) = @_;
	my $a = $hash{'a'};
	addToHash( 'mods.genre' , $a );
}

sub getThesis_degree_apartment
{
	my (%hash) = @_;
	my $w = $hash{'w'};

	
	addToHash( 'thesis.degree.department' , $w );
}

sub addToHash
{
	my ($key,$value) = @_;
	if( $row{$key} )
	{
		$row{$key} = $row{$key} . "||" . $value;
	}
	else
	{
		$row{$key} = $value;
	}
}

sub autofill
{
	#my () = @_;
	foreach my $x (@ARGV) 
   	{
   		if ($x eq '-a' || $x eq '-A') 
   	  	{
   	  		#addToHash( 'dc.publisher' , "Assumption University" );
   	  		addToHash( 'dc.type' , "Text" );
   	  		addToHash( 'dc.format.mimetype' , "application/pdf" );
   	  		addToHash( 'dc.rights' , "This work is protected by copyright. Reproduction or distribution of the work in any format is prohibited without written permission of the copyright owner." );
   	  		addToHash( 'mods.genre' , "Journal Article" );
   	  		#addToHash( 'thesis.degree.level' , "Doctoral" );
   	  		#addToHash( 'thesis.degree.grantor' , "Assumption University" );
   	  	}
   	} 	
}

sub plog 
{
	my ($log) = @_;
	foreach my $x (@ARGV) 
	{
	  	if ($x eq '-d' || $x eq '-D') 
	  	{
	  	 	$DEBUG = 1;
	  	}
	}
	print OUTPUT_LOG $log if $DEBUG; 	
}









































 
    