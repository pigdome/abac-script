#use File::Find::Rule;
use strict;
use warnings;

my $basedir = $ARGV[0];
my $add_permissions = $ARGV[1];

opendir(HOMEDIR,$basedir) || die ("Unable to open directory"); 
while (my $filename = readdir(HOMEDIR)) 
{ 
    my $subdir = $basedir. "/" . $filename;print $subdir . "\n";
    open FILE, "> $subdir/contents" or next;
    if( -d $subdir )
    {
        opendir(SUBDIR,$subdir) || die ("Unable to open directory");
        while (my $fname = readdir(SUBDIR)) 
        {
            if( index( $fname,".pdf") >= 0 or index( $fname,".PDF") >= 0 )
            {
                if( $add_permissions eq '-a' )
                {
                    print FILE $fname . "\tpermissions: -r 'INTRANET',-r 'MEMBER'\n";
                }
                else
                {
                    print FILE $fname . "\n";
                }
            }
        }
        closedir(SUBDIR);
    }
    close( FILE );
}
closedir(HOMEDIR); 