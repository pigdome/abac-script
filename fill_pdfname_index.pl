use strict;
use MARC::Batch;
use MARC::Record;
use MARC::File::USMARC;
use utf8;
use Text::CSV;

my $index = "/srv/work/pdf_renamed/index/index-fin-map.csv";

my $csv = Text::CSV->new ( { binary => 0 } ) or die "Cannot use CSV: ".Text::CSV->error_diag ();
open(my $fh_index, '<:encoding(UTF-8)', $index) or die "Could not open file '$index' $!";

my @rows_index = ();

#-----------------------------------------------------

while ( my $row = $csv->getline( $fh_index ) )
{
	$row->[30] =~ s/^\s+|\s+$//g;
	my $uri = $row->[30];
	$uri =~ s/Full Text Available  : http:\/\/www.aulibrary.au.edu\/multim1\/ABAC_Pub//g;
	$uri =~	s/Full Text Available : http:\/\/www.aulibrary.au.edu\/multim1\/ABAC_Pub//g;
	$uri =~	s/Full Text Available  : http:\/\/www.library.au.edu//g;
	$uri =~	s/Full Text Avalible : http:\/\/www.aulibrary.au.edu\/multim1\/ABAC_Pub//g;
	$uri =~	s/Cover and Table of Contents  : http:\/\/www.library.au.edu\/elib\/213003.html ||//g;
	$uri =~	s/Full Text Avalible  : http:\/\/www.aulibrary.au.edu\/multim1\/ABAC_Pub//g;
	$uri =~	s/Full Text Avalible : http:\/\/www.aulibrary.au.edu\/multim1\/ABAC_Pub//g;
	#
	my $pdfname = getPDFName($uri);
	$pdfname =~ s/\/\/Au-Journal-of-Technology//g;
	$row->[1] = $pdfname if $row->[1] ne 'filename';
	push @rows_index , $row;
}
close $fh_index;
#-----------------------------------------------------

open( my $OUTPUT_CSV, '> /srv/work/pdf_renamed/index/index-fin-map-pdf.csv' ) or die $!;
binmode($OUTPUT_CSV, ':utf8');
$csv->eol ("\r\n");
$csv->print ($OUTPUT_CSV, $_) for @rows_index;
close( $OUTPUT_CSV );


sub getPDFName 
{
	my ($uri) = @_;
	my @sp = split('/',$uri);
	my $parent_dir = $sp[1];
	my $pdfname = $sp[2];#print $pdfname . "\n";
    
	$parent_dir =~ s/||//g;
	$parent_dir =~ s/AU-Journal-Of-Management/AU-Journal-of-Management/g;
	$parent_dir = "The-Journal-of-Risk-Management-And-Insurance" if $parent_dir eq "Journal-of-Risk-Management-And-Insurance";
	$parent_dir =~ s/ABAC-journal/ABAC-Journal/g;
	$parent_dir =~ s/AU-Journal-of-Technology/Au-Journal-of-Technology/g;

	opendir(BASEDIR,"/srv/work/pdf_renamed/index/" . $parent_dir) or die $!;
	while (my $filename = readdir(BASEDIR)) 
	{
		$pdfname = lc $pdfname;
		if( lc $filename =~ /$pdfname/ )
		{
			return "/" . $parent_dir . "/" . $filename;
		} 
	}
	return "";
}