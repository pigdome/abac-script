#use File::Find::Rule;
use strict;
use warnings;

my $basedir = $ARGV[0];
opendir(HOMEDIR,$basedir) || die ("Unable to open directory");
my $count = $ARGV[1];

while (my $filename = readdir(HOMEDIR)) 
{ 
    my $subdir = $basedir. "/" . $filename;
    if( -d $subdir )
    {
        if( length($filename) > 3 ) 
        {
            open(my $fh,$subdir . "/collections") or next;
            while (my $row = <$fh>) 
            {
                $row =~ s/^\s+|\s+$//g;
                if( index($row,"/") >= 0 )
                {
                    #print $subdir . " : $row\n";
                    system("mkdir","/srv/work/collection/$row") if ! -e "/srv/work/collection/$row";
                    #system("cp","-R",$subdir,"/srv/work/collection/$row/" . $count++);
                    system("mv",$subdir,"/srv/work/collection/$row/" . $count++);    
                }
            }
        }
    } 
}
closedir(HOMEDIR); 