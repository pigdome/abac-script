use strict;
use warnings;

my $srcdir = $ARGV[0];
my $destdir = $ARGV[1];
my $offset = $ARGV[2];	
    
	opendir(DIR, ( $srcdir ) ) or die $!;
	while (my $file = readdir(DIR)) 
	{
        if ( index( lc $file , '.pdf') >= 0 ) 
        {
            my $path_src = $srcdir . "/" . $file;
            my $path_des = $destdir . "/$offset" . lc $file;

        	system("ln","-s",$path_src,$path_des);
            if ($? == -1) 
            {
                print "failed to execute: $!\n";
            }
            else 
            {
                printf "copy pdf use symbolic link $path_src to $path_des\n";
            }
        }
    }
	closedir(DIR);
  

