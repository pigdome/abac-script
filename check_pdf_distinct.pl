#use File::Find::Rule;
use strict;
use warnings;

my $basedir = $ARGV[0];
my $compdir = $ARGV[1];
my $csv = $ARGV[2];
my @rows_base = ();
my @rows_comp = ();

open( OUTPUT_CSV, '> ' . $ARGV[2] ) or die $!;
opendir(BASEDIR,$basedir) || die ("Unable to open directory"); 
opendir(COMPDIR,$compdir) || die ("Unable to open directory"); 

while (my $filename = readdir(BASEDIR)) 
{ 
    push @rows_base , $filename;
}
while (my $filename = readdir(COMPDIR)) 
{ 
    push @rows_comp , $filename;
}

closedir(BASEDIR);
closedir(COMPDIR);

print OUTPUT_CSV "\"pdf_name\",\"status\"\n";
my $count_match = 0;
my $count_unmatch = 0;
foreach my $file_base (@rows_base) 
{
    my $match = 0;
    unless ( $file_base =~ /.pdf/ or $file_base =~ /.PDF/ ){next;}

    foreach my $file_comp (@rows_comp) 
    {
        if( lc $file_base eq lc $file_comp )
        {
            $match = 1;
        }
    }
    if( $match )
    {
        print OUTPUT_CSV "\"$file_base\",\"match\"\n";
        $count_match++;
    }
    else
    {
        print OUTPUT_CSV "\"$file_base\",\"unmatch\"\n";
        $count_unmatch++;
    }
}
print OUTPUT_CSV "\"total match\",\"$count_match\"\n";
print OUTPUT_CSV "\"total unmatch\",\"$count_unmatch\"\n";
close OUTPUT_CSV;